package uz.devops.websocketchat.chat;

public enum MessageType {

    JOIN,

    CHAT,

    LEAVE
}
